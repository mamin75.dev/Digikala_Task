import {StyleSheet} from 'react-native';

export const sharedStyles = StyleSheet.create({
  screen: {
    flex: 1,
    backgroundColor: '#eee',
    alignItems: 'center',
  },
});
