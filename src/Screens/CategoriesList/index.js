import React from 'react';
import {View, Text} from 'react-native';

import {sharedStyles} from '../../Shared';
import styles from './styles';
import {List} from './Components';

export default React.memo(({navigation}) => {
  return (
    <View style={[sharedStyles.screen, styles.screen]}>
      <Text style={styles.title}>MOVIE COLLECTION</Text>
      <List navigation={navigation} />
    </View>
  );
});
