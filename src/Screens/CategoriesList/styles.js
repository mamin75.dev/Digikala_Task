import {StyleSheet} from 'react-native';
import {height, scaleFontSize} from '../../Utils';

export default StyleSheet.create({
  screen: {
    backgroundColor: 'rgba(69,126,255,0.2)',
  },
  title: {
    fontSize: scaleFontSize(36),
    fontWeight: 'bold',
    marginTop: height * 0.02,
  },
});
