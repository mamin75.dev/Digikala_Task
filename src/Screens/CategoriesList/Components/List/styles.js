import {StyleSheet} from 'react-native';
import {height} from '../../../../Utils';

export default StyleSheet.create({
  list: {
    marginTop: height * 0.02,
    paddingBottom: height * 0.02,
  },
});
