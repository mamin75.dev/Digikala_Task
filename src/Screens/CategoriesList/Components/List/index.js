import React from 'react';
import {FlatList, ActivityIndicator} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {getGenresList} from '../../../../Redux/actions';
import {GenresItem} from '../../../../Components';
import {colors, height} from '../../../../Utils';
import styles from './styles';

export const List = React.memo(({navigation}) => {
  const loading = useSelector(state => state.app.appLoading);
  const genres = useSelector(state => state.movie.genres.data);
  const currentPage = useSelector(
    state => state.movie.genres.pager.current_page,
  );
  const totalPages = useSelector(state => state.movie.genres.pager.total_pages);
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(getGenresList(1, 20));
  }, []);

  const _renderItem = ({item, index}) => {
    return (
      <GenresItem
        containerStyle={{marginVertical: height * 0.01}}
        {...item}
        navigation={navigation}
      />
    );
  };

  const _onEndReached = () => {
    if (currentPage < totalPages) {
      dispatch(getGenresList(currentPage + 1, 20));
    }
  };

  const _renderFooter = () => {
    if (loading) {
      return <ActivityIndicator size={'large'} color={colors.blue} />;
    }
    return null;
  };

  return (
    <FlatList
      data={genres}
      renderItem={_renderItem}
      keyExtractor={(item, index) => index.toString()}
      numColumns={3}
      onEndReached={_onEndReached}
      contentContainerStyle={styles.list}
      onEndReachedThreshold={0.8}
      ListFooterComponent={_renderFooter}
    />
  );
});
