import {StyleSheet} from 'react-native';
import {scaleFontSize} from '../../Utils/window';

export default StyleSheet.create({
  title: {
    fontSize: scaleFontSize(36),
    fontWeight: 'bold',
  },
});
