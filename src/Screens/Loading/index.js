import React from 'react';
import {View, Text} from 'react-native';

import {sharedStyles} from '../../Shared';
import {Loader} from '../../Components';
import styles from './styles';

export default React.memo(props => {
  return (
    <View style={sharedStyles.screen}>
      <Text style={styles.title}>MOVIE COLLECTION</Text>
      <Loader />
    </View>
  );
});
