import {StyleSheet} from 'react-native';
import {colors} from '../../Utils/colors';
import {height, scaleFontSize} from '../../Utils/window';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.blue,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: scaleFontSize(36),
    fontWeight: 'bold',
    color: colors.white,
  },
  username: {
    marginTop: height * 0.1,
  },
});
