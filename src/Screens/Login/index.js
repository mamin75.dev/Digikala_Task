import React from 'react';
import {View, Text} from 'react-native';
import {useDispatch} from 'react-redux';
import Snackbar from 'react-native-snackbar';
import styles from './styles';
import {Input, Button} from '../../Components';
import {colors} from '../../Utils';
import {authActions} from '../../Redux/actions';
import {SIGN_IN} from '../../Redux/types';

export default React.memo(props => {
  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');
  const dispatch = useDispatch();

  const usernameChange = text => {
    setUsername(text);
  };

  const passwordChange = text => {
    setPassword(text);
  };

  const loginPress = () => {
    if (!username || !password) {
      Snackbar.show({
        text: 'Please complete form!',
        textColor: colors.red,
        duration: Snackbar.LENGTH_SHORT,
      });
      return true;
    }
    dispatch(
      authActions({
        type: SIGN_IN,
        payload: {
          username: username,
          password: password,
        },
      }),
    );
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>L O G I N</Text>
      <Input
        containerStyle={styles.username}
        label={'Username'}
        onChangeText={usernameChange}
        value={username}
      />
      <Input
        label={'Password'}
        secureTextEntry={true}
        onChangeText={passwordChange}
        value={password}
      />
      <Button onPress={loginPress} title={'Login'} />
    </View>
  );
});
