import {StyleSheet} from 'react-native';
import {height, scaleFontSize, width} from '../../../../Utils';

export default StyleSheet.create({
  title: {
    fontSize: scaleFontSize(26),
    textAlign: 'left',
    width: width,
    fontWeight: 'bold',
    marginTop: height * 0.02,
    marginLeft: width * 0.04,
    marginBottom: height * 0.01,
  },
  showMore: {
    width: width * 0.45,
    height: height * 0.4,
  },
});
