import React from 'react';
import {View, FlatList, Text, ActivityIndicator} from 'react-native';
import {useSelector} from 'react-redux';
import {colors, height, width} from '../../../../Utils';
import {MoviesItem} from '../MoviesItem';
import styles from './styles';

export const MoviesList = React.memo(props => {
  const loading = useSelector(state => state.app.appLoading);
  const movies = useSelector(state => {
    if (state.movie.movies.data.length > 10) {
      return state.movie.movies.data.slice(0, 10);
    }
    return state.movie.movies.data;
  });

  const _renderItem = ({item, index}) => {
    return <MoviesItem {...item} />;
  };

  return (
    <View style={{height: height * 0.5, paddingLeft: width * 0.05}}>
      <Text style={styles.title}>Featured Movies</Text>
      {loading ? (
        <ActivityIndicator
          size={'large'}
          color={colors.blue}
          style={styles.loading}
        />
      ) : (
        <FlatList
          data={movies}
          renderItem={_renderItem}
          keyExtractor={(item, index) => index.toString()}
          horizontal
        />
      )}
    </View>
  );
});
