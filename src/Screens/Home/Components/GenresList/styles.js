import {StyleSheet} from 'react-native';
import {colors, height, scaleFontSize, width} from '../../../../Utils';

export default StyleSheet.create({
  title: {
    fontSize: scaleFontSize(26),
    fontWeight: 'bold',
    marginTop: height * 0.02,
    marginLeft: width * 0.04,
    width: width,
    textAlign: 'left',
    marginBottom: height * 0.02,
  },
  showMore: {
    position: 'absolute',
    backgroundColor: colors.blue,
    right: width * 0.06,
    top: height * 0.02,
    paddingHorizontal: width * 0.02,
    paddingVertical: height * 0.005,
    borderRadius: width * 0.02,
  },
});
