import React from 'react';
import {
  FlatList,
  Text,
  View,
  TouchableNativeFeedback,
  ActivityIndicator,
} from 'react-native';
import {useSelector} from 'react-redux';
import {GenresItem} from '../../../../Components';
import {colors, width} from '../../../../Utils';
import {screens} from '../../../../Config';
import styles from './styles';

export const GenresList = React.memo(({navigation}) => {
  const genres = useSelector(state => {
    if (state.movie.genres.data.length > 10) {
      return state.movie.genres.data.slice(0, 10);
    }
    return state.movie.genres.data;
  });

  const loading = useSelector(state => state.app.appLoading);

  const _renderItem = ({item, index}) => {
    return <GenresItem {...item} navigation={navigation} />;
  };

  const _navigateToGenres = () => {
    navigation.navigate(screens.CategoriesList);
  };

  return (
    <View style={{paddingLeft: width * 0.05}}>
      <Text style={styles.title}>Movie Genres</Text>
      {loading ? (
        <ActivityIndicator
          size={'large'}
          color={colors.blue}
          style={styles.loading}
        />
      ) : (
        <>
          <FlatList
            data={genres}
            renderItem={_renderItem}
            keyExtractor={(item, index) => index.toString()}
            horizontal
          />
          <TouchableNativeFeedback onPress={_navigateToGenres}>
            <View style={styles.showMore}>
              <Text style={{color: colors.white}}>Show all</Text>
            </View>
          </TouchableNativeFeedback>
        </>
      )}
    </View>
  );
});
