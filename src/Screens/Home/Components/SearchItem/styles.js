import {StyleSheet} from 'react-native';
import {colors, height, scaleFontSize, width} from '../../../../Utils';

export default StyleSheet.create({
  item: {
    marginVertical: height * 0.006,
    flexDirection: 'row',
    alignItems: 'center',
    width: width * 0.95,
    alignSelf: 'center',
    height: height * 0.08,
  },
  title: {
    fontSize: scaleFontSize(22),
    fontWeight: 'bold',
  },
  image: {
    width: width * 0.15,
    height: width * 0.15,
    borderRadius: width * 0.075,
  },
  textContainer: {
    borderBottomWidth: 1,
    borderColor: colors.gray,
    height: '100%',
    marginLeft: width * 0.05,
    justifyContent: 'center',
    paddingRight: width * 0.3,
    width: '100%',
  },
  imdb: {
    position: 'absolute',
    right: 0,
    paddingVertical: 2,
    paddingHorizontal: width * 0.02,
    borderRadius: 5,
  },
  rating: {
    color: colors.white,
  },
});
