import React from 'react';
import {View, Text, Image} from 'react-native';
import {colors} from '../../../../Utils';
import styles from './styles';

export const SearchItem = React.memo(
  ({title, poster, Director, imdb_rating}) => {
    const temp = poster.split(' =>');
    const image = temp[0] + ':' + temp[1];
    return (
      <View style={styles.item}>
        <Image source={{uri: image}} style={styles.image} />
        <View style={styles.textContainer}>
          <Text numberOfLines={1} style={styles.title}>
            {title}
          </Text>
          <Text style={styles.director}>{Director[0]}</Text>
        </View>
        {!isNaN(parseFloat(imdb_rating)) ? (
          <View
            style={[
              styles.imdb,
              {
                backgroundColor:
                  imdb_rating > 7
                    ? colors.green
                    : imdb_rating > 4
                    ? colors.orange
                    : colors.red,
              },
            ]}>
            <Text style={styles.rating}>{imdb_rating}</Text>
          </View>
        ) : null}
      </View>
    );
  },
);
