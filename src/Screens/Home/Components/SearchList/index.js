import React from 'react';
import {FlatList, View} from 'react-native';
import {useSelector} from 'react-redux';
import {SearchItem} from '../SearchItem';
import {height} from '../../../../Utils';

export const SearchList = React.memo(props => {
  const movies = useSelector(state => state.movie.movies.data);

  const _renderItem = ({item, index}) => {
    return <SearchItem {...item} />;
  };

  return (
    <View style={{marginTop: height * 0.02}}>
      <FlatList
        data={movies}
        renderItem={_renderItem}
        keyExtractor={(item, index) => index.toString()}
      />
    </View>
  );
});
