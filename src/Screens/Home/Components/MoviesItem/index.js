import React from 'react';
import {Text, Image, View} from 'react-native';
import CardView from 'react-native-cardview';
import {colors, width} from '../../../../Utils';
import styles from './styles';

export const MoviesItem = React.memo(
  ({title, poster, Director, year, imdb_rating}) => {
    const temp = poster.split(' =>');
    const image = temp[0] + ':' + temp[1];
    return (
      <CardView
        cardElevatinon={2}
        cornerRadius={width * 0.02}
        style={styles.item}>
        <Image source={{uri: image}} style={styles.image} />
        <Text numberOfLines={1} style={styles.title}>
          {title}
        </Text>
        <Text numberOfLines={1} style={styles.director}>
          {Director[0]}
        </Text>
        <Text style={styles.year}>{year}</Text>
        {!isNaN(parseFloat(imdb_rating)) ? (
          <View
            style={[
              styles.imdb,
              {
                backgroundColor:
                  imdb_rating > 7
                    ? colors.green
                    : imdb_rating > 4
                    ? colors.orange
                    : colors.red,
              },
            ]}>
            <Text style={styles.rating}>{imdb_rating}</Text>
          </View>
        ) : null}
      </CardView>
    );
  },
);
