import {StyleSheet} from 'react-native';
import {height, scaleFontSize, width, colors} from '../../../../Utils';

export default StyleSheet.create({
  item: {
    width: width * 0.45,
    height: height * 0.4,
    marginHorizontal: width * 0.01,
  },
  image: {
    height: height * 0.3,
    width: width * 0.45,
    resizeMode: 'cover',
  },
  title: {
    marginLeft: width * 0.02,
    marginTop: height * 0.005,
    fontSize: scaleFontSize(20),
    fontWeight: 'bold',
  },
  director: {
    marginLeft: width * 0.02,
    // marginTop: height * 0.005,
    fontSize: scaleFontSize(18),
  },
  year: {
    textAlign: 'right',
    marginRight: width * 0.02,
    marginTop: height * 0.01,
  },
  imdb: {
    position: 'absolute',
    paddingVertical: 1,
    paddingHorizontal: width * 0.01,
    right: width * 0.01,
    top: width * 0.01,
    borderRadius: 5,
  },
  rating: {
    color: colors.white,
  },
});
