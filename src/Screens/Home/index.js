import React from 'react';
import {View, Text} from 'react-native';

import {sharedStyles} from '../../Shared';
import styles from './styles';
import {useDispatch} from 'react-redux';
import {getGenresList, getMovies} from '../../Redux/actions';
import {SearchBox} from '../../Components';
import {MoviesList, GenresList, SearchList} from './Components';
import {CLEAR_MOVIES} from '../../Redux/types';

export default React.memo(({navigation}) => {
  const [search, setSearch] = React.useState('');
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(getGenresList(1, 10));
  }, []);

  React.useEffect(() => {
    if (search.length === 0) {
      dispatch(getMovies());
    }
  }, [search]);

  const changeSearchValue = text => {
    dispatch({
      type: CLEAR_MOVIES,
      payload: {},
    });
    setSearch(text);
  };

  return (
    <View style={[sharedStyles.screen, styles.screen]}>
      <Text style={styles.title}>MOVIE COLLECTION</Text>
      <SearchBox
        value={search}
        onChangeText={changeSearchValue}
        placeholder={'Type here ...'}
      />
      {search.length > 0 ? (
        <SearchList />
      ) : (
        <>
          <MoviesList />
          <GenresList navigation={navigation} />
        </>
      )}
    </View>
  );
});
