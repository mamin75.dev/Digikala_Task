import React from 'react';
import {ActivityIndicator, FlatList} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {getMovies} from '../../../../Redux/actions';
import {colors} from '../../../../Utils';
import {ListItem} from '../ListItem';
import styles from './styles';

export const List = React.memo(({navigation, genre}) => {
  const loading = useSelector(state => state.app.appLoading);
  const movies = useSelector(state => state.movie.movies.data);
  const currentPage = useSelector(
    state => state.movie.movies.pager.current_page,
  );
  const totalPages = useSelector(state => state.movie.movies.pager.total_pages);
  const dispatch = useDispatch();

  const _renderItem = ({item, index}) => {
    return <ListItem {...item} />;
  };

  const _onEndReached = () => {
    if (currentPage < totalPages) {
      dispatch(getMovies(currentPage + 1, 10, 'English', '', genre));
    }
  };

  const _renderFooter = () => {
    if (loading) {
      return <ActivityIndicator size={'large'} color={colors.blue} />;
    }
    return null;
  };

  return (
    <FlatList
      data={movies}
      renderItem={_renderItem}
      keyExtractor={(item, index) => index.toString()}
      onEndReached={_onEndReached}
      contentContainerStyle={styles.list}
      onEndReachedThreshold={0.8}
      ListFooterComponent={_renderFooter}
    />
  );
});
