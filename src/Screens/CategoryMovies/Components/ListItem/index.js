import React from 'react';
import {Image, Text, View, FlatList} from 'react-native';
import CardView from 'react-native-cardview';
import StarRating from 'react-native-star-rating';
import {colors, height, width} from '../../../../Utils';
import styles from './styles';

export const ListItem = React.memo(
  ({title, poster, Director, imdb_rating, plot, genres}) => {
    const temp = poster.split(' =>');
    const image = temp[0] + ':' + temp[1];

    const _renderGenres = ({item, index}) => {
      return <Text style={styles.genre}>#{item}</Text>;
    };

    return (
      <CardView
        cardElevatinon={2}
        cornerRadius={width * 0.02}
        style={styles.item}>
        <Image source={{uri: image}} style={styles.image} />
        <View style={styles.content}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.director} numberOfLines={1}>
            {Director.toString()}
          </Text>
          <View style={styles.genresList}>
            <FlatList
              data={genres}
              renderItem={_renderGenres}
              keyExtractor={(item, index) => index.toString()}
              horizontal
            />
          </View>
          <Text style={styles.plot} numberOfLines={3}>
            {plot}
          </Text>
          {!isNaN(parseFloat(imdb_rating)) ? (
            <View style={styles.star}>
              <StarRating
                disabled={true}
                fullStarColor={colors.orange}
                halfStarColor={colors.orange}
                emptyStarColor={colors.orange}
                maxStars={5}
                starSize={width * 0.05}
                rating={parseFloat(imdb_rating)}
              />
            </View>
          ) : null}
        </View>
      </CardView>
    );
  },
);
