import {StyleSheet} from 'react-native';
import {colors, height, scaleFontSize, width} from '../../../../Utils';

export default StyleSheet.create({
  item: {
    width: width * 0.95,
    height: height * 0.35,
    marginVertical: height * 0.005,
    overflow: 'hidden',
    flexDirection: 'row',
  },
  image: {
    flex: 2.5,
    height: height * 0.35,
  },
  content: {
    flex: 3.5,
    padding: width * 0.02,
  },
  title: {
    fontSize: scaleFontSize(26),
    color: colors.blue,
    fontWeight: 'bold',
  },
  director: {
    fontSize: scaleFontSize(21),
    fontWeight: 'bold',
  },
  plot: {
    fontSize: scaleFontSize(18),
  },
  genre: {
    backgroundColor: colors.blue,
    marginHorizontal: width * 0.01,
    height: height * 0.04,
    color: colors.white,
    textAlignVertical: 'center',
    paddingHorizontal: width * 0.01,
    borderRadius: width * 0.02,
  },
  genresList: {
    height: height * 0.06,
    marginVertical: height * 0.02,
  },
  star: {
    // position: 'absolute',
    alignSelf: 'center',
    marginTop: height * 0.02
  }
});
