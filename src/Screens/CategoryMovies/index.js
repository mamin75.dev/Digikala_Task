import React from 'react';
import {View, Text} from 'react-native';

import {sharedStyles} from '../../Shared';
import {List} from './Components';
import styles from './styles';

export default React.memo(({route}) => {
  return (
    <View style={[sharedStyles.screen, styles.screen]}>
      <Text style={styles.title}>MOVIE COLLECTION</Text>
      <List genre={route.params.genre} />
    </View>
  );
});
