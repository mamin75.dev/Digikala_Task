import {APP_LOADING, BUTTON_LOADING} from '../types';

const INITIAL_STATE = {
  btnLoading: false,
  appLoading: false,
};

export const appReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case BUTTON_LOADING: {
      return {
        ...state,
        btnLoading: action.payload.value,
      };
    }
    case APP_LOADING: {
      return {
        ...state,
        appLoading: action.payload.value,
      };
    }
    default:
      return state;
  }
};
