import {CLEAR_MOVIES, MOVIE_GENRES, MOVIES} from '../types';
import {removeDuplicateFromArray} from '../actions';

const INITIAL_STATE = {
  genres: {
    data: [],
    pager: null,
  },
  movies: {
    data: [],
    pager: null,
  },
};

export const movieReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case MOVIE_GENRES: {
      const data = removeDuplicateFromArray(
        state.genres.data.concat(action.payload.genres),
        'id',
      );
      return {
        ...state,
        genres: {
          data: data,
          pager: action.payload.pager,
        },
      };
    }
    case MOVIES: {
      const data = state.movies.data.concat(action.payload.movies);
      return {
        ...state,
        movies: {
          data: data,
          pager: action.payload.pager,
        },
      };
    }
    case CLEAR_MOVIES: {
      return {
        ...state,
        movies: {
          data: [],
          pager: {},
        },
      };
    }
    default: {
      return state;
    }
  }
};
