import {ACCESS_TOKEN} from '../types';

const INITIAL_STATE = {
  access: null,
};

export const authReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ACCESS_TOKEN: {
      return {
        ...state,
        access: action.payload.access,
      };
    }
    default:
      return state;
  }
};
