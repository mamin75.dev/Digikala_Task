import {APP_LOADING, BUTTON_LOADING} from '../types';

export const toggleButtonLoading = (show = false) => {
  return dispatch => {
    /**
     * show loading on button
     */
    dispatch({
      type: BUTTON_LOADING,
      payload: {value: show || false},
    });
  };
};

export const toggleLoading = (show = false) => {
  return dispatch => {
    /**
     * show loading on screen
     */
    dispatch({
      type: APP_LOADING,
      payload: {value: show || false},
    });
  };
};

export const removeDuplicateFromArray = (data = [], key = 'id') => {
  const uniqBy = require('lodash/uniqBy');
  return uniqBy(data, key);
};
