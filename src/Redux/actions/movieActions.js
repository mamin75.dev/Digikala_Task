import {axios} from '../../Config';
import {MOVIE_GENRES, MOVIES} from '../types';
import {toggleLoading} from '.';

export const getGenresList = (page, rows) => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      const params = {
        page: page ? page : 1,
        rows: rows ? rows : 10,
      };
      dispatch(toggleLoading(true));
      axios({
        method: 'get',
        url: '/genres/',
        params: params,
      })
        .then(res => {
          if (res.data.hasOwnProperty('status') && res.data.status == 200) {
            dispatch({
              type: MOVIE_GENRES,
              payload: {
                genres: res.data.data.genres,
                pager: res.data.data.pager.pager,
              },
            });
          }
          resolve(true);
        })
        .catch(e => {
          reject(false);
        })
        .finally(() => {
          dispatch(toggleLoading(false));
        });
    });
  };
};

export const getMovies = (
  page = 1,
  rows = 10,
  lang = 'English',
  title = '',
  genre = '',
) => {
  return dispatch => {
    return new Promise((resolve, reject) => {
      let params = {
        page: page,
        rows: rows,
      };
      lang.length > 0 ? (params['language[0]'] = lang) : null;
      title.length > 0 ? (params.title = title) : null;
      genre.length > 0 ? (params['genre[0]'] = genre) : null;
      dispatch(toggleLoading(true));
      axios({
        method: 'get',
        url: '/movies/',
        params: params,
      })
        .then(res => {
          if (res.data.hasOwnProperty('status') && res.data.status == 200) {
            dispatch({
              type: MOVIES,
              payload: {
                movies: res.data.data.movies,
                pager: res.data.data.pager.pager,
              },
            });
          }
          resolve(true);
        })
        .catch(e => {
          reject(false);
        })
        .finally(() => {
          dispatch(toggleLoading(false));
        });
    });
  };
};
