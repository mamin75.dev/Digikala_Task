import Snackbar from 'react-native-snackbar';
import {ACCESS_TOKEN, SIGN_IN} from '../types';
import {axios} from '../../Config';
import {asyncStorageSetItem, colors} from '../../Utils';
import {toggleButtonLoading} from '.';

export const authActions = ({type, payload}) => {
  return dispatch => {
    switch (type) {
      case SIGN_IN: {
        return new Promise((resolve, reject) => {
          const username = payload.username;
          const password = payload.password;
          dispatch(toggleButtonLoading(true));
          axios({
            method: 'post',
            _addToken: false,
            url: '/login/',
            data: {
              username: username,
              password: password,
            },
          })
            .then(async res => {
              if (
                res.data.hasOwnProperty('status') &&
                res.data.status === 200
              ) {
                const token = res.data.data.token;
                await asyncStorageSetItem('access', token);
                dispatch({
                  type: ACCESS_TOKEN,
                  payload: {access: token},
                });
                Snackbar.show({
                  text: 'Login successful!',
                  textColor: colors.green,
                  duration: Snackbar.LENGTH_SHORT,
                });
                return resolve(true);
              }
              Snackbar.show({
                text: 'Username or Password incorrect!',
                textColor: colors.red,
                duration: Snackbar.LENGTH_SHORT,
              });
              return reject(false);
            })
            .catch(e => {
              Snackbar.show({
                text: 'Unknown error!',
                textColor: colors.red,
                duration: Snackbar.LENGTH_SHORT,
              });
              if (__DEV__) {
                console.log('log:: login', e);
              }
              reject(false);
            })
            .finally(() => {
              dispatch(toggleButtonLoading(false));
            });
        });
      }
    }
  };
};
