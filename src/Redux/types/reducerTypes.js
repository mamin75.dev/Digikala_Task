export const ACCESS_TOKEN = 'ACCESS_TOKEN';
export const BUTTON_LOADING = 'BUTTON_LOADING';
export const APP_LOADING = 'APP_LOADING';
export const MOVIE_GENRES = 'MOVIE_GENRES';
export const MOVIES = 'MOVIES';
export const CLEAR_MOVIES = 'CLEAR_MOVIES';
