import {StyleSheet} from 'react-native';
import {height, width} from '../../Utils';

export default StyleSheet.create({
  container: {
    width: width * 0.5,
    height: height * 0.1,
    position: 'absolute',
    top: height * 0.6,
  },
  lottie: {transform: [{scale: 2}]},
});
