import React from 'react';
import {View} from 'react-native';
import LottieView from 'lottie-react-native';
import styles from './styles';

export const Loader = React.memo(props => {
  return (
    <View style={styles.container}>
      <LottieView
        source={require('../../../assets/json/loading.json')}
        autoPlay
        loop
        style={styles.lottie}
      />
    </View>
  );
});
