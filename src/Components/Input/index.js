import React from 'react';
import {View, TextInput, Text} from 'react-native';
import styles from './styles';

export const Input = React.memo(
  ({
    value,
    onChangeText,
    placeholder,
    label,
    labelStyle,
    containerStyle,
    inputContainerStyle,
  }) => {
    return (
      <View style={[styles.container, containerStyle]}>
        <Text style={[styles.label, labelStyle]}>{label}</Text>
        <View style={[styles.inputContainer, inputContainerStyle]}>
          <TextInput
            value={value}
            onChangeText={onChangeText}
            placeholder={placeholder}
            style={styles.input}
          />
        </View>
      </View>
    );
  },
);
