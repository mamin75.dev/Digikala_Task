import {StyleSheet} from 'react-native';
import {height, scaleFontSize, width, colors} from '../../Utils';

export default StyleSheet.create({
  container: {marginTop: height * 0.05},
  inputContainer: {
    backgroundColor: colors.white,
    width: width * 0.9,
    borderRadius: width * 0.02,
    height: height * 0.07,
    marginTop: height * 0.01,
  },
  input: {
    padding: 0,
    margin: 0,
    flex: 1,
    borderRadius: width * 0.02,
    width: width * 0.9,
    paddingHorizontal: width * 0.04,
    textAlignVertical: 'center',
    fontSize: scaleFontSize(20),
  },
  label: {
    fontSize: scaleFontSize(24),
    color: colors.white,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
