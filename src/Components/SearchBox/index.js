import React from 'react';
import {
  TextInput,
  View,
  TouchableWithoutFeedback,
  Text,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {useDispatch, useSelector} from 'react-redux';
import {CLEAR_MOVIES} from '../../Redux/types';
import {getMovies} from '../../Redux/actions';
import {width, colors} from '../../Utils';
import styles from './styles';

export const SearchBox = ({value, onChangeText, placeholder}) => {
  const loading = useSelector(state => state.app.appLoading);
  const dispatch = useDispatch();

  const searchMovie = () => {
    dispatch({
      type: CLEAR_MOVIES,
      payload: {},
    });
    dispatch(getMovies(1, 10, 'English', value));
  };

  return (
    <View style={styles.container}>
      <Icon name={'search'} size={width * 0.08} color={colors.blue} />
      <TextInput
        value={value}
        onChangeText={onChangeText}
        placeholder={placeholder}
        style={styles.input}
      />
      {value.length > 0 ? (
        <TouchableWithoutFeedback onPress={searchMovie}>
          <View style={styles.search}>
            {loading ? <ActivityIndicator /> : <Text>Search</Text>}
          </View>
        </TouchableWithoutFeedback>
      ) : null}
    </View>
  );
};
