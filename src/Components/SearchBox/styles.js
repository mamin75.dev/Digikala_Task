import {StyleSheet} from 'react-native';
import {height, scaleFontSize, width, colors} from '../../Utils';

export default StyleSheet.create({
  container: {
    width: width * 0.9,
    marginTop: height * 0.03,
    borderColor: colors.blue,
    borderWidth: 2,
    borderRadius: width * 0.2,
    paddingHorizontal: width * 0.03,
    paddingVertical: height * 0.01,
    flexDirection: 'row',
    alignItems: 'center',
  },
  input: {
    padding: 0,
    margin: 0,
    flex: 1,
    paddingHorizontal: width * 0.04,
    textAlignVertical: 'center',
    fontSize: scaleFontSize(24),
  },
  search: {
    backgroundColor: colors.blue,
    paddingVertical: height * 0.005,
    paddingHorizontal: width * 0.01,
    borderRadius: width * 0.02,
  },
});
