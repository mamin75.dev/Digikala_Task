import {StyleSheet} from 'react-native';
import {scaleFontSize, width} from '../../Utils';

export default StyleSheet.create({
  item: {
    width: width * 0.25,
    height: width * 0.25,
    marginHorizontal: width * 0.01,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: scaleFontSize(20),
    fontWeight: 'bold',
  },
});
