import React from 'react';
import {Text, TouchableWithoutFeedback} from 'react-native';
import CardView from 'react-native-cardview';
import {useDispatch} from 'react-redux';
import {CLEAR_MOVIES} from '../../Redux/types';
import {getMovies} from '../../Redux/actions';
import {screens} from '../../Config';
import {width} from '../../Utils';
import styles from './styles';

export const GenresItem = React.memo(({name, navigation, containerStyle}) => {
  const dispatch = useDispatch();

  const navigateToMovies = () => {
    dispatch({type: CLEAR_MOVIES, payload: {}});
    navigation.navigate(screens.CategoryMovies, {
      genre: name,
    });
    dispatch(getMovies(1, 10, 'English', '', name));
  };

  return (
    <TouchableWithoutFeedback onPress={navigateToMovies}>
      <CardView
        cardElevatinon={2}
        cornerRadius={width * 0.125}
        style={[styles.item, containerStyle]}>
        <Text style={styles.title}>{name}</Text>
      </CardView>
    </TouchableWithoutFeedback>
  );
});
