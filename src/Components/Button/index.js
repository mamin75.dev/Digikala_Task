import React from 'react';
import {
  Platform,
  View,
  TouchableNativeFeedback,
  TouchableOpacity,
  Text,
  ActivityIndicator,
} from 'react-native';
import {useSelector} from 'react-redux';
import {colors} from '../../Utils';
import styles from './styles';

const Touchable =
  Platform.OS === 'android' ? TouchableNativeFeedback : TouchableOpacity;

export const Button = React.memo(({onPress, title}) => {
  const loading = useSelector(state => state.app.btnLoading);
  return (
    <Touchable onPress={onPress}>
      <View style={styles.container}>
        {loading ? (
          <ActivityIndicator color={colors.black} />
        ) : (
          <Text style={styles.text}>{title}</Text>
        )}
      </View>
    </Touchable>
  );
});
