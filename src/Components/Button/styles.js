import {StyleSheet} from 'react-native';
import {height, scaleFontSize, width, colors} from '../../Utils';

export default StyleSheet.create({
  container: {
    width: width * 0.9,
    height: height * 0.06,
    backgroundColor: colors.orange,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: height * 0.08,
    borderRadius: width * 0.02,
  },
  text: {
    fontSize: scaleFontSize(24),
  },
});
