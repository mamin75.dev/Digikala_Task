import axios from 'axios';
import {store} from './store';
import {asyncStorageGetItem} from '../Utils';

const BASE_URL = 'https://www.digikala.com/front-end';

const instance = axios.create({
  baseURL: BASE_URL,
});

// instance.defaults.baseURL = BASE_URL;

if (__DEV__) {
  instance.interceptors.request.use(request => {
    return request;
  });

  instance.interceptors.response.use(response => {
    return response;
  });
}

instance.interceptors.request.use(
  async config => {
    let access = null;
    if (store) {
      const state = store.getState();
      access = state.auth.access;
    }
    if (config?._addToken !== false) {
      if (access) {
        config.headers.Authorization = access;
        return config;
      } else {
        const accessToken = await asyncStorageGetItem('access');
        if (!accessToken) {
          return Promise.reject(config);
        }
        config.headers.Authorization = accessToken;
        return Promise.resolve(config);
      }
    } else {
      return config;
    }
  },
  error => Promise.reject(error),
);

export {instance as axios};
