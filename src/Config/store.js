import {createStore, applyMiddleware, combineReducers} from 'redux';

import ReduxThunk from 'redux-thunk';

import {appReducer, authReducer, movieReducer} from '../Redux/reducers';

const rootReducer = combineReducers({
  auth: authReducer,
  app: appReducer,
  movie: movieReducer,
});

export const store = createStore(rootReducer, applyMiddleware(ReduxThunk));
