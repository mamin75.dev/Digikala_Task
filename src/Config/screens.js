export const screens = {
  Loading: 'Loading',
  Login: 'Login',
  Home: 'Home',
  CategoriesList: 'CategoriesList',
  CategoryMovies: 'CategoryMovies',
};
