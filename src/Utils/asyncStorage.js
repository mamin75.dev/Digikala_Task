import AsyncStorage from '@react-native-async-storage/async-storage';

export const asyncStorageSetItem = async (key, value) => {
  const _key = typeof key === 'string' ? key : key.toString();
  const _value = typeof value === 'string' ? value : JSON.stringify(value);
  try {
    await AsyncStorage.setItem(_key, _value);
  } catch (e) {
    if (__DEV__) {
      console.log('log:: asyncStorageSetItem', e);
    }
  }
};

export const asyncStorageGetItem = async key => {
  const _key = typeof key === 'string' ? key : key.toString();
  try {
    const value = await AsyncStorage.getItem(_key);
    if (value !== null) {
      return value;
    }
  } catch (e) {
    if (__DEV__) {
      console.log('log:: asyncStorageGetItem', e);
    }
  }
};
