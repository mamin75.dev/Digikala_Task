export const colors = {
  blue: '#457eff',
  white: '#fff',
  orange: '#ffc145',
  black: '#000',
  green: '#1c8116',
  red: '#e01313',
  gray: '#aaa',
};
