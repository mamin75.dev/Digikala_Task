import {Dimensions, Platform} from 'react-native';

export const width = Dimensions.get('window').width;
export const height = Dimensions.get('window').height;

export const screenWidthSize = Math.min(width, height);
export const screenHeightSize = Math.max(width, height);

export const scaleFontSize = (size, maxSize = Infinity) => {
  // const newSize = (screenWidthSize * size * 0.8) / 504;
  const newSize =
    (screenHeightSize * size * 0.9) / Platform.select({android: 896, ios: 896});
  return newSize > maxSize ? maxSize : newSize;
};
