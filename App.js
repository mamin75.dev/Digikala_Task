import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

/**
 * import screens
 */
import Loading from './src/Screens/Loading';
import Login from './src/Screens/Login';
import Home from './src/Screens/Home';
import CategoriesList from './src/Screens/CategoriesList';
import CategoryMovies from './src/Screens/CategoryMovies';

/**
 * import screen_names
 */
import {screens} from './src/Config';

import {asyncStorageGetItem} from './src/Utils';
import {store} from './src/Config/store';
import {ACCESS_TOKEN} from './src/Redux/types';
import {Provider, useSelector} from 'react-redux';

const Stack = createNativeStackNavigator();

const Navigator = () => {
  const [loading, setLoading] = React.useState(true);
  const access = useSelector(state => state.auth.access);
  React.useEffect(() => {
    const bootstrapAsync = async () => {
      try {
        const token = await asyncStorageGetItem('access');
        if (token) {
          saveTokenToRedux(token);
        }
        /**
         * for beauty :)
         */
        setTimeout(() => {
          setLoading(false);
        }, 2000);
      } catch (e) {
        // Restoring token failed
        if (__DEV__) {
          console.log('log:: getToken', e);
        }
      }
    };
    bootstrapAsync();
  }, []);

  const saveTokenToRedux = token => {
    store.dispatch({
      type: ACCESS_TOKEN,
      payload: {access: token},
    });
  };

  if (loading) {
    return <Loading />;
  }

  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        {access ? (
          <>
            <Stack.Screen name={screens.Home} component={Home} />
            <Stack.Screen
              name={screens.CategoriesList}
              component={CategoriesList}
            />
            <Stack.Screen
              name={screens.CategoryMovies}
              component={CategoryMovies}
            />
          </>
        ) : (
          <>
            <Stack.Screen name={screens.Login} component={Login} />
          </>
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

const App = () => {
  return (
    <Provider store={store}>
      <Navigator />
    </Provider>
  );
};

export default App;
